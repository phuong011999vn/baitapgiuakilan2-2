﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QuanLyDTMVC.Models;
using System.Net.Http;

namespace QuanLyDTMVC.Controllers
{
    public class PhonesController : Controller
    {
        // GET: Phones
        public ActionResult Index()
        {
            IEnumerable<Phone> phones = null;


            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:7482/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Phones");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<Phone>>();
                    readTask.Wait();

                    phones = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    phones = Enumerable.Empty<Phone>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(phones);
        }

        // GET: Phones/Details/5
        public ActionResult Details(int id)
        {
            Phone phones = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:7482/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Phones?id=" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Phone>();
                    readTask.Wait();

                    phones = readTask.Result;
                }
            }
            return View(phones);
        }

        // GET: Phones/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Phones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult create(Phone phones)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:7482/api/");

                //HTTP POST
                var postTask = client.PostAsJsonAsync<Phone>("Phones", phones);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");

            return View(phones);
        }
        //EDIT
        public ActionResult Edit(int id)
        {
            Phone phones = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:7482/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Phones?id=" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Phone>();
                    readTask.Wait();

                    phones = readTask.Result;
                }
            }
            return View(phones);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Phone phones)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:7482/api/");

                //HTTP POST
                var putTask = client.PutAsJsonAsync<Phone>("Phones", phones);
                putTask.Wait();

                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    return RedirectToAction("Index");
                }
            }
            return View(phones);
        }

        public ActionResult Delete(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:7482/api/");

                //HTTP DELETE
                var deleteTask = client.DeleteAsync("Phones/" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    return RedirectToAction("Index");
                }
            }

            return RedirectToAction("Index");
        }
    }
}
