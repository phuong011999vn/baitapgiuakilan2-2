﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuanLyDTMVC.Models
{
    public class Phone
    {
        public int id { get; set; }
        public string Model { get; set; }
        public Nullable<double> Price { get; set; }
        public string GeneralNote { get; set; }
    }
}